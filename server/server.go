package server

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/nikitian/ad-server/ads"
	"bitbucket.org/nikitian/ad-server/config"
	"github.com/avct/uasurfer"
	log "github.com/mgutz/logxi/v1"
	routing "github.com/qiangxue/fasthttp-routing"
	"github.com/valyala/fasthttp"
)

const (
	routeGetAds = "/get-ads"

	cookieFreq = "userFrequency"
	geoHeader  = "X-GEO"
)

func Run() {
	go func() {
		router := routing.New()
		router.Get(routeGetAds, getAdsHandler)

		router.NotFound(notFoundHandler)

		err := fasthttp.ListenAndServe(":"+config.Data.Server.Port, router.HandleRequest)

		if err != nil {
			panic("Api server start failed: " + err.Error())
		}
	}()
}

func getUserHistory(cookieData []byte) []ads.UserFreq {
	var userFreq []ads.UserFreq
	err := json.Unmarshal(cookieData, &userFreq)
	if err != nil {
		return []ads.UserFreq{}
	}

	oldFreq := userFreq
	userFreq = []ads.UserFreq{}
	for _, freq := range oldFreq {
		if time.Since(time.Unix(freq.Date, 0)) < 24*time.Hour {
			userFreq = append(userFreq, freq)
		}
	}

	return userFreq
}

func setUserHistory(ctx *routing.Context, userFreq []ads.UserFreq) {
	userFreqCookie := &fasthttp.Cookie{}
	userFreqCookie.SetKey(cookieFreq)
	userFreqCookie.SetExpire(time.Now().Add(config.Data.Server.FreqExpireHours * time.Hour))
	userFreqCookie.SetPath("/")
	userFreqJson, err := json.Marshal(userFreq)
	if err != nil {
		userFreqJson = []byte("[]")
	}
	userFreqCookie.SetValueBytes(userFreqJson)
	ctx.Response.Header.SetCookie(userFreqCookie)
}

func getAdsHandler(ctx *routing.Context) error {
	userFreq := getUserHistory(ctx.Request.Header.Cookie(cookieFreq))

	userAgent := string(ctx.Request.Header.UserAgent())
	ua := uasurfer.Parse(userAgent)

	geo := string(ctx.Request.Header.Peek(geoHeader))
	if geo == "" {
		geo = "N/A"
	}

	result, err := ads.GetAd(ua.Browser.Name.String(), ua.OS.Name.String(), geo, userFreq)
	if err != nil {
		result = ads.Result{
			Status: "Not Found",
		}
		log.Debug("Banner not found")
	} else {
		userFreq = append(userFreq, ads.UserFreq{
			Id:   result.Id,
			Date: time.Now().Unix(),
		})
		log.Info("View",
			"bannerId", result.Id,
			"UserIp", ctx.RemoteIP(),
		)
	}

	setUserHistory(ctx, userFreq)
	response, err := json.Marshal(result)

	if err != nil {
		log.Error(fmt.Sprintf("error on create resonse %v", err))
		return notFoundHandler(ctx)
	}

	ctx.Response.SetBody(response)

	return nil
}

func notFoundHandler(ctx *routing.Context) error {
	ctx.Response.Reset()
	ctx.SetStatusCode(404)
	ctx.SetBodyString("404 Not Found")

	return nil
}
