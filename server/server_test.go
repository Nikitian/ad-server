package server

import (
	"reflect"
	"strconv"
	"testing"
	"time"

	"bitbucket.org/nikitian/ad-server/ads"
)

func Test_getUserHistory(t *testing.T) {
	type args struct {
		cookieData []byte
	}
	tests := []struct {
		name string
		args args
		want []ads.UserFreq
	}{
		{
			name: "1",
			args: args{
				cookieData: []byte("[{\"i\": 1, \"t\": 123}]"),
			},
			want: []ads.UserFreq{},
		},
		{
			name: "2",
			args: args{
				cookieData: []byte("[{\"i\": 1, \"t\": " + strconv.Itoa(int(time.Now().Unix())) + "}]"),
			},
			want: []ads.UserFreq{
				{
					Id:   1,
					Date: time.Now().Unix(),
				},
			},
		},
		{
			name: "3",
			args: args{
				cookieData: []byte("[{\"i\": 1, \"t\": 123},{\"i\": 1, \"t\": " + strconv.Itoa(int(time.Now().Unix())) + "}]"),
			},
			want: []ads.UserFreq{
				{
					Id:   1,
					Date: time.Now().Unix(),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getUserHistory(tt.args.cookieData); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getUserHistory() = %v, want %v", got, tt.want)
			}
		})
	}
}
