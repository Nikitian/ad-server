package log

import (
	"bitbucket.org/nikitian/ad-server/config"
	log "github.com/mgutz/logxi/v1"
)

func Init() {
	log.KeyMap = &log.KeyMapping{
		Level:     "level",
		Message:   "message",
		Name:      "name",
		PID:       "pid",
		Time:      "time",
		CallStack: "callstack",
	}
	log.DefaultLog = log.NewLogger3(
		log.NewConcurrentWriter(NewUDPWriter(config.Data.Server.Log.Host)),
		config.Data.Server.Log.Facility,
		log.NewJSONFormatter(config.Data.Server.Log.Facility),
	)
}
