package log

import (
	"io"
	"net"
)

type UDPWriter struct {
	io.Writer

	addr string
}

func NewUDPWriter(addr string) io.Writer {
	return &UDPWriter{
		addr: addr,
	}
}

func (w *UDPWriter) Write(p []byte) (n int, err error) {
	conn, err := net.Dial("udp", w.addr)
	if err != nil {
		return
	}

	return conn.Write(p)
}
