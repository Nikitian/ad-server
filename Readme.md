##Build

`go build adserver.go`

##Run

Run ELK `docker-compose -f docker/docker-compose.yml up -d`

Run application `LOGXI=* go run adserver.go`

###Trouble shootings
If ELK starts failed, try this:

Linux: `sudo sysctl -w vm.max_map_count=262144`

Mac & Win: Increase memory limit for docker

## Tests
`go test ./...`


## Kibana with views
`http://localhost:5601/app/kibana#/discover?_g=(refreshInterval:(pause:!t,value:0),time:(from:now%2Fd,mode:quick,to:now%2Fd))&_a=(columns:!(_source),index:'796176a0-6653-11e9-83f8-0b817b42682d',interval:auto,query:(language:lucene,query:'log.message:View'),sort:!('@timestamp',desc))`