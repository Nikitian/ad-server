module github.com/nikitian/adserver

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/avct/uasurfer v0.0.0-20190308134847-43c6f9a90eeb
	github.com/go-ozzo/ozzo-routing v2.1.4+incompatible // indirect
	github.com/golang/gddo v0.0.0-20190419222130-af0f2af80721 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/mgutz/logxi v0.0.0-20161027140823-aebf8a7d67ab
	github.com/qiangxue/fasthttp-routing v0.0.0-20160225050629-6ccdc2a18d87
	github.com/spf13/viper v1.3.2
	github.com/valyala/fasthttp v1.2.0
)
