package config

import (
	"fmt"

	"github.com/spf13/viper"
)

func Init() {
	viper.AddConfigPath("./")
	viper.SetConfigName("config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	err = viper.Unmarshal(&Data)
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}
