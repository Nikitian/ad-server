package config

import "time"

var Data struct {
	Server struct {
		Port            string        `mapstructure:"port"`
		FreqExpireHours time.Duration `mapstructure:"freq_cookie_expire_hours"`
		Log             struct {
			Host     string `mapsctructure:"host"`
			Facility string `mapsctructure:"facility"`
		} `mapsctructure:"log"`
	} `mapstructure:"server"`
	Ads []Ad `mapstructure:"ads"`
}

type Ad struct {
	Id       int      `mapstructure:"id",json:"id"`
	Name     string   `mapstructure:"name",json:"name"`
	Freq     string   `mapstructure:"freq",json:"freq"`
	Geos     []string `mapstructure:"geos",json:"geos"`
	Browsers []string `mapstructure:"browsers",json:"browsers"`
	Oses     []string `mapstructure:"oses",json:"oses"`
}
