package main

import (
	"context"

	"bitbucket.org/nikitian/ad-server/config"
	"bitbucket.org/nikitian/ad-server/log"
	"bitbucket.org/nikitian/ad-server/server"
)

func main() {
	ctx := context.TODO()
	config.Init()
	log.Init()
	server.Run()

	<-ctx.Done()
}
