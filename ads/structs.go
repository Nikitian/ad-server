package ads

type Result struct {
	Status string
	Id     int
	Name   string
}

type UserFreq struct {
	// Short names for store in cookie
	Id   int   `json:"i"`
	Date int64 `json:"t"`
}
