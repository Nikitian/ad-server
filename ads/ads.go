package ads

import (
	"errors"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/nikitian/ad-server/config"
)

func GetAd(browserName string, osName string, geo string, userFreq []UserFreq) (result Result, err error) {
	ads := shuffle(config.Data.Ads)
	for _, ad := range ads {
		if checkGeo(ad.Geos, geo) && checkBrowser(ad.Browsers, browserName) && checkOs(ad.Oses, osName) && checkFreq(ad.Freq, ad.Id, userFreq) {
			return Result{
				Status: "OK",
				Id:     ad.Id,
				Name:   ad.Name,
			}, nil
		}
	}
	err = errors.New("not found any ads")

	return
}

// "Rotation" for small-data
func shuffle(vals []config.Ad) []config.Ad {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	ret := make([]config.Ad, len(vals))
	n := len(vals)
	for i := 0; i < n; i++ {
		randIndex := r.Intn(len(vals))
		ret[i] = vals[randIndex]
		vals = append(vals[:randIndex], vals[randIndex+1:]...)
	}
	return ret
}

func checkOs(expectOs []string, os string) bool {
	if len(expectOs) == 0 {
		return true
	}
	for _, expect := range expectOs {
		if "OS"+expect == os {
			return true
		}
	}

	return false
}

func checkBrowser(expectBrowser []string, browser string) bool {
	if len(expectBrowser) == 0 {
		return true
	}
	for _, expect := range expectBrowser {
		if "Browser"+expect == browser {
			return true
		}
	}

	return false
}

func checkGeo(expectGeo []string, geo string) bool {
	if len(expectGeo) == 0 {
		return true
	}
	for _, expect := range expectGeo {
		if expect == geo {
			return true
		}
	}

	return false
}

func checkFreq(expectFreq string, adId int, userFreq []UserFreq) bool {
	const defaultTimeLimit = 24
	if len(userFreq) == 0 {
		return true
	}
	var err error
	expectSlice := strings.Split(expectFreq, "/")
	timeLimit := defaultTimeLimit
	if len(expectSlice) > 1 {
		timeLimit, err = strconv.Atoi(expectSlice[1])
		if err != nil {
			timeLimit = defaultTimeLimit
		}
	}
	expectFreqInt, err := strconv.Atoi(expectSlice[0])
	if err != nil {
		expectFreqInt = 0
	}
	return expectFreqInt > calcCountViews(adId, userFreq, time.Duration(timeLimit)*time.Hour)
}

func calcCountViews(adId int, userFreq []UserFreq, timeLimit time.Duration) int {
	cnt := 0

	for _, freq := range userFreq {
		if freq.Id == adId && freq.Date >= time.Now().Add(-timeLimit).Unix() {
			cnt++
		}
	}

	return cnt
}
