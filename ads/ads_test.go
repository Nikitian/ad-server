package ads

import (
	"reflect"
	"testing"
	"time"

	"bitbucket.org/nikitian/ad-server/config"
)

func Test_checkOs(t *testing.T) {
	type args struct {
		expectOs []string
		os       string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "1",
			args: args{
				expectOs: []string{},
				os:       "Xxx",
			},
			want: true,
		},
		{
			name: "2",
			args: args{
				expectOs: []string{"Xxx"},
				os:       "OSXxx",
			},
			want: true,
		},
		{
			name: "3",
			args: args{
				expectOs: []string{"Yyy", "Xxx"},
				os:       "OSXxx",
			},
			want: true,
		},
		{
			name: "4",
			args: args{
				expectOs: []string{"Yyy", "Xxx"},
				os:       "Xxx",
			},
			want: false,
		},
		{
			name: "5",
			args: args{
				expectOs: []string{"Yyy"},
				os:       "Xxx",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkOs(tt.args.expectOs, tt.args.os); got != tt.want {
				t.Errorf("checkOs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_checkBrowser(t *testing.T) {
	type args struct {
		expectBrowser []string
		browser       string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "1",
			args: args{
				expectBrowser: []string{},
				browser:       "Xxx",
			},
			want: true,
		},
		{
			name: "2",
			args: args{
				expectBrowser: []string{"Xxx"},
				browser:       "BrowserXxx",
			},
			want: true,
		},
		{
			name: "3",
			args: args{
				expectBrowser: []string{"Yyy", "Xxx"},
				browser:       "BrowserXxx",
			},
			want: true,
		},
		{
			name: "4",
			args: args{
				expectBrowser: []string{"Yyy", "Xxx"},
				browser:       "Xxx",
			},
			want: false,
		},
		{
			name: "5",
			args: args{
				expectBrowser: []string{"Yyy"},
				browser:       "Xxx",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkBrowser(tt.args.expectBrowser, tt.args.browser); got != tt.want {
				t.Errorf("checkBrowser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_checkGeo(t *testing.T) {
	type args struct {
		expectGeo []string
		geo       string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "1",
			args: args{
				expectGeo: []string{},
				geo:       "Xxx",
			},
			want: true,
		},
		{
			name: "2",
			args: args{
				expectGeo: []string{"Xxx"},
				geo:       "Xxx",
			},
			want: true,
		},
		{
			name: "3",
			args: args{
				expectGeo: []string{"Yyy", "Xxx"},
				geo:       "Xxx",
			},
			want: true,
		},
		{
			name: "4",
			args: args{
				expectGeo: []string{"Yyy"},
				geo:       "Xxx",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkGeo(tt.args.expectGeo, tt.args.geo); got != tt.want {
				t.Errorf("checkGeo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_checkFreq(t *testing.T) {
	type args struct {
		expectFreq string
		adId       int
		userFreq   []UserFreq
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "1",
			args: args{
				expectFreq: "",
				adId:       1,
				userFreq:   []UserFreq{},
			},
			want: true,
		},
		{
			name: "2",
			args: args{
				expectFreq: "1/24",
				adId:       1,
				userFreq:   []UserFreq{},
			},
			want: true,
		},
		{
			name: "3",
			args: args{
				expectFreq: "2/24",
				adId:       1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
			},
			want: true,
		},
		{
			name: "4",
			args: args{
				expectFreq: "2/24",
				adId:       1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   2,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
			},
			want: true,
		},
		{
			name: "5",
			args: args{
				expectFreq: "1/24",
				adId:       1,
				userFreq: []UserFreq{
					{
						Id:   2,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
			},
			want: true,
		},
		{
			name: "6",
			args: args{
				expectFreq: "2/24",
				adId:       1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
			},
			want: false,
		},
		{
			name: "7",
			args: args{
				expectFreq: "1/24",
				adId:       1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
			},
			want: false,
		},
		{
			name: "8",
			args: args{
				expectFreq: "1/1",
				adId:       1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Hour).Unix(),
					},
					{
						Id:   2,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
			},
			want: false,
		},
		{
			name: "9",
			args: args{
				expectFreq: "1/1",
				adId:       1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Hour - time.Second).Unix(),
					},
					{
						Id:   2,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkFreq(tt.args.expectFreq, tt.args.adId, tt.args.userFreq); got != tt.want {
				t.Errorf("checkFreq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calcCountViews(t *testing.T) {
	type args struct {
		adId      int
		userFreq  []UserFreq
		timeLimit time.Duration
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "1",
			args: args{
				adId:      1,
				userFreq:  []UserFreq{},
				timeLimit: time.Hour,
			},
			want: 0,
		},
		{
			name: "2",
			args: args{
				adId: 1,
				userFreq: []UserFreq{
					{
						Id:   2,
						Date: int64(1),
					},
				},
				timeLimit: time.Hour,
			},
			want: 0,
		},
		{
			name: "3",
			args: args{
				adId: 1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
				timeLimit: time.Hour,
			},
			want: 1,
		},
		{
			name: "4",
			args: args{
				adId: 1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
				timeLimit: time.Hour,
			},
			want: 2,
		},
		{
			name: "5",
			args: args{
				adId: 1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   2,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
				timeLimit: time.Hour,
			},
			want: 2,
		},
		{
			name: "6",
			args: args{
				adId: 1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   1,
						Date: time.Now().Add(-(59 * time.Minute)).Unix(),
					},
					{
						Id:   1,
						Date: time.Now().Add(-time.Hour).Unix(),
					},
					{
						Id:   2,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
				timeLimit: time.Hour,
			},
			want: 3,
		},
		{
			name: "7",
			args: args{
				adId: 1,
				userFreq: []UserFreq{
					{
						Id:   1,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
					{
						Id:   1,
						Date: time.Now().Add(-time.Hour - time.Second).Unix(),
					},
					{
						Id:   2,
						Date: time.Now().Add(-time.Minute).Unix(),
					},
				},
				timeLimit: time.Hour,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calcCountViews(tt.args.adId, tt.args.userFreq, tt.args.timeLimit); got != tt.want {
				t.Errorf("calcCountViews() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_shuffle(t *testing.T) {
	type args struct {
		vals []config.Ad
	}
	tests := []struct {
		name string
		args args
		want []config.Ad
	}{
		{
			name: "1",
			args: args{
				vals: []config.Ad{},
			},
			want: []config.Ad{},
		},
		{
			name: "2",
			args: args{
				vals: []config.Ad{
					{
						Id:       1,
						Name:     "123",
						Freq:     "1/24",
						Geos:     []string{"DE", "RU"},
						Browsers: []string{"Chrome"},
						Oses:     []string{"Android", "Linux"},
					},
				},
			},
			want: []config.Ad{
				{
					Id:       1,
					Name:     "123",
					Freq:     "1/24",
					Geos:     []string{"DE", "RU"},
					Browsers: []string{"Chrome"},
					Oses:     []string{"Android", "Linux"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shuffle(tt.args.vals); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("shuffle() = %v, want %v", got, tt.want)
			}
		})
	}
}
